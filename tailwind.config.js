/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors')
module.exports = {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
    ],
    theme: {
        extend: {},
        colors: {
            primary: 'var(--color-primary)',
            secondary: 'var(--color-secondary)',
            accent: 'var(--color-accent)',
            blue: colors.blue,
            gray: colors.gray,
            red: colors.rose,
            green: colors.green,
            yellow: colors.yellow,
            white: '#fff',
            black: '#000',
            transparent: 'transparent'
        }
    },
    plugins: [],
}
