import axios from 'axios';
import {useToast} from 'vue-toast-notification';
const $toast = useToast();
axios.interceptors.response.use(ResponseSuccessHandler, ResponseErrorHandler)

window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.withCredentials = true;


function ResponseSuccessHandler(response){
    console.log('sucessssssssss')
    console.log(response.data)
    if (response.data.message) {
       $toast.open({
            message: response.data.message,
            type: 'success',
            position: 'top'
        });
    }
    return response;

}

function ResponseErrorHandler(error){
    $toast.open({
            message: error.response.data.message,
            type: 'error',
            position: 'top'
        });
    return Promise.reject(error);
}



