import('./bootstrap.js')
import { createSSRApp, h } from 'vue'
import { createInertiaApp } from '@inertiajs/vue3'
import 'vue-toast-notification/dist/theme-sugar.css';

const app = createInertiaApp({
    resolve: name => {
        const pages = import.meta.glob('./Pages/**/*.vue', { eager: true })
        return pages[`./Pages/${name}.vue`]
    },
    setup({ el, App, props, plugin }) {
            createSSRApp({ render: () => h(App, props) })
            .use(plugin)
            .mount(el)
    }
})
export {app}
