<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\TicketController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth')->group(function () {
    Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
    Route::post('/tickets', [TicketController::class, 'store'])->name('get-tickets');
    Route::get('/tickets', [TicketController::class, 'index'])->name('get-tickets');
    Route::get('/tickets/{id}', [TicketController::class, 'show'])->name('get-ticket');
});
// Route::post('/register', [RegisterController::class, 'register'])->name('register');

Route::post('/login', [LoginController::class, 'login']);
Route::post('/redeem', [TicketController::class, 'redeem'])->name('redeem-ticket');
Route::get('/tickets/qr/{id}', [TicketController::class, 'getSingleByQr'])->name('get-ticket-by-qr');
