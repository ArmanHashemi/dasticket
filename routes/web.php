<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TicketController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;


Route::get('/', function () {
    return Inertia::render('App', ['auth' => auth()->user()]);
})->name('app');

Route::get('/qr/{ticket_id}', [TicketController::class,'generate'])->name('qr');

Route::get('/scan/{ticket_id}', function () {
    return Inertia::render('Scan', ['ticket_id' => request('ticket_id', '')]);
})->name('scan');

Route::middleware('auth')->group(function () {
    Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

    Route::get('/generate', function () {return Inertia::render('Generate');})->name('dashboard');

    Route::get('/dashboard', [DashboardController::class, 'show'])->name('dashboard');

});





