<?php

namespace App\Http\Controllers;
use App\Models\Ticket;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class TicketController extends Controller
{
    use ValidatesRequests;

    public function index()
    {
        return Ticket::all();
    }
    public function store(Request $request)
    {
        $create = Ticket::create($request->all());
        if ($create){
            return response()->json(['message' => 'عملیات با موفقیت انجام شد']);
        }else{
            return response()->json(['message' => 'خطا در ثبت کوپن'],500);
        }
    }
    public function show(Request $request)
    {
        $id = $request->route('id');

        $item = Ticket::where('id', $id)->where('status', 0)->first();

        if (!$item) {
            // Handle the case where the item doesn't exist or its status isn't 1.
            return response()->json(['message' => 'کد تخفیف یافت نشد'], 404);
        }

        return $item;
    }
    public function getSingleByQr(Request $request)
    {
        $id = $request->route('id');

        $item = Ticket::where('qr_code', $id)->first();

        if (!$item) {
            // Handle the case where the item doesn't exist or its status isn't 1.
            return response()->json(['message' => 'کد تخفیف یافت نشد'], 404);
        }

        return $item;
    }
    public function redeem(Request $request)
    {
         $key = 'dast12345';
        // $id = $request->route('id');
        $validator = Validator::make($request->all(), [
            'secret_key' => 'required',
            'ticket_id' => 'required|min:3',
            'factor_number' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }


        $secret = $request->post('secret_key');
        if ($secret != $key){
            return response()->json(['message' => 'رمز عبور اشتباه است'], 401);

        }
        $ticket_id = $request->post('ticket_id');
        $factor_number = $request->post('factor_number');

        $item = Ticket::where('qr_code', $ticket_id)->where('status', 0)->first();

        if (!$item) {
            // Handle the case where the update was unsuccessful or the item doesn't exist
            return response()->json(['message' => 'کد تخفیف اشتباه است یا قبلا استفاده شده'], 404);
        }

         Ticket::where('qr_code', $ticket_id)->update([
             'factor_number'=> $factor_number,
            'status' => 1
        ]);
        return response()->json(['message' => 'عملیات با موفقیت انجام شد']);
    }
    public function generate(Request $request)
    {
        $isPercent= false;
        $isCount= false;
        $ticket_id = request('ticket_id');
        $item = Ticket::where('qr_code', $ticket_id)->first();
        if ($item['percent']) {
            $isPercent = true;
        }
        if ($item['count']) {
            $isCount = true;
        }

        return Inertia::render('Qr', [
            'ticket_id' => request('ticket_id', ''),
            'isPercent' => $isPercent,
            'isCount' => $isCount,
            'percent' => $item['percent'],
            'count' => $item['count'],
        ]);
    }


}

