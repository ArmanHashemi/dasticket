<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function show()
    {

        $ticketController = new TicketController();
        $tickets = $ticketController->index();
        return Inertia::render('Dashboard',
            [
                'auth' => auth()->user(),
                'tickets' => $tickets
            ]
        );

    }
}
